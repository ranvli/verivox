﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TariffCalcAPI.TariffCalcAPI.Models.Enums;

namespace TariffCalcAPI.TariffCalcAPI.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public string UnitName { get; set; }
        
        public double BasePrice { get; set; }
        public double MaximumUnits { get; set; }
        public double ExtraUnitPricePerUnit { get; set; }

        public int ContractLenghtMonths { get; set; }
    }
}
