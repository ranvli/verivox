﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TariffCalcAPI.TariffCalcAPI.Models.Enums
{
    public enum ProductCategory
    {
        Electricity = 0,
        Gas = 1,
        Water = 2,
        MobileTelephony = 3,
        LandTelephony = 3
    }
}
