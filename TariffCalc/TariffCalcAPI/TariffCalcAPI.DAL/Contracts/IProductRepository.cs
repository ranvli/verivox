﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TariffCalcAPI.Dto;
using TariffCalcAPI.TariffCalcAPI.Models;

namespace TariffCalcAPI.TariffCalcAPI.DAL.Contracts
{
    public interface IProductRepository : IRepository<Product>
    {
        //here you can add the additional definition methods additional to CRUDs for the repository
        ICollection<ProductPricingDto> CalculatePricing(ProductCalculatePricingDto productCalculatePricingDto);
    }
}
