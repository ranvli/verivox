﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TariffCalcAPI.TariffCalcAPI.DAL.Contracts
{
    //Base repository with CRUDs
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);

        int CommitChanges();
        Task<int> CommitChangesAsync();
    }
}
