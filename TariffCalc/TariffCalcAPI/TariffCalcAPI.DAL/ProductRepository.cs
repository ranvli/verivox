﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TariffCalcAPI.Dto;
using TariffCalcAPI.TariffCalcAPI.DAL.Context;
using TariffCalcAPI.TariffCalcAPI.DAL.Contracts;
using TariffCalcAPI.TariffCalcAPI.Models;

namespace TariffCalcAPI.TariffCalcAPI.DAL
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        VeriVoxContext verivoxContext = null;
        public ProductRepository(VeriVoxContext context) : base(context)
        {
            verivoxContext = context;
        }

        //additional methods here...

        public ICollection<ProductPricingDto> CalculatePricing(ProductCalculatePricingDto productCalculatePricingDto)
        {
            List<ProductPricingDto> productPricings = new List<ProductPricingDto>();

            //first of all, we retrieve all the products available of the same category
            var products = verivoxContext.Products.Where(p => p.ProductCategory == productCalculatePricingDto.ProductCategory);

            foreach (var product in products)
            {
                ProductPricingDto productPricing = new ProductPricingDto();
                productPricing.ProductName = product.ProductName;

                //calculate extra units invoicing
                double extraUnitCosts = (productCalculatePricingDto.QuantityConsumption - product.MaximumUnits) * product.ExtraUnitPricePerUnit;
                extraUnitCosts = extraUnitCosts < 0 ? 0 : extraUnitCosts;

                //calculate base pricing
                double basePricingCosts = product.BasePrice * (productCalculatePricingDto.Months / product.ContractLenghtMonths);

                //Totalize annual cost
                productPricing.AnnualCost = basePricingCosts + extraUnitCosts;
                
                productPricings.Add(productPricing);
            }


            return productPricings;
        }

        

    }
}
