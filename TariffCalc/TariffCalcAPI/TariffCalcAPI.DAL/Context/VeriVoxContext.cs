﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TariffCalcAPI.TariffCalcAPI.Models;

namespace TariffCalcAPI.TariffCalcAPI.DAL.Context
{
    public partial class VeriVoxContext : DbContext
    {
        public virtual DbSet<Product> Products { get; set; }
        
        public VeriVoxContext(DbContextOptions<VeriVoxContext> options) : base(options)
        {
            //ensure to create inMemory seed data
            this.Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //inMemory to test this POC
            optionsBuilder.UseInMemoryDatabase("VeriVoxInMemory");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            //Create Basic seed data
           
            builder.Entity<Product>().HasData
            (
                new Product
                {
                    ProductId = 1,
                    ProductCategory = Models.Enums.ProductCategory.Electricity,
                    BasePrice = 5,
                    ExtraUnitPricePerUnit = 0.22,
                    MaximumUnits = 0,
                    ContractLenghtMonths = 1,
                    ProductName = "Basic electricity tariff",
                    UnitName = "kWh"
                },
                new Product
                {
                    ProductId = 2,
                    ProductCategory = Models.Enums.ProductCategory.Electricity,
                    BasePrice = 800,
                    ExtraUnitPricePerUnit = 0.30,
                    ContractLenghtMonths = 12,
                    MaximumUnits = 4000,
                    ProductName = "Packaged tariff”",
                    UnitName = "kWh"
                }
            );
        }
    }


}
