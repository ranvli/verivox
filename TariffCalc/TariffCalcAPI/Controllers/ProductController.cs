﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TariffCalcAPI.Dto;
using TariffCalcAPI.Dto.Base;
using TariffCalcAPI.TariffCalcAPI.Models;
using TariffCalcAPI.TariffCalcAPI.Services.Contracts;

namespace TariffCalcAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("product-get-all")]
        [SwaggerResponse((int)HttpStatusCode.OK, "If products were found", typeof(ResponseDto<IEnumerable<Product>>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Returns for any uncontroller exception")]
        public IActionResult ProductGetAll()
        {
            ResponseDto<IEnumerable<Product>> response = new ResponseDto<IEnumerable<Product>>();

            try
            {
                response.Data = _productService.GetAll();
                return Ok(response);
            }
            catch (Exception e)
            {
                response.Error.ExceptionMessage = e.Message;
                response.Error.Message = "Internal Error";
                response.Error.Code = 1;
                return BadRequest(response);
                throw;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("price-calculator")]
        [SwaggerResponse((int)HttpStatusCode.OK, "If price was calculated", typeof(ResponseDto<IEnumerable<ProductPricingDto>>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Returns for any uncontroller exception")]
        public IActionResult ProductPriceCalculator(ProductCalculatePricingDto productCalculatePricingDto)
        {
            ResponseDto<IEnumerable<ProductPricingDto>> response = new ResponseDto<IEnumerable<ProductPricingDto>>();

            try
            {
                response.Data = _productService.CalculatePricing(productCalculatePricingDto);
                return Ok(response);
            }
            catch (Exception e)
            {
                response.Error.ExceptionMessage = e.Message;
                response.Error.Message = "Internal Error";
                response.Error.Code = 1;
                return BadRequest(response);
                throw;
            }
        }
    }
}