﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TariffCalcAPI.Dto;
using TariffCalcAPI.TariffCalcAPI.DAL.Contracts;
using TariffCalcAPI.TariffCalcAPI.Models;
using TariffCalcAPI.TariffCalcAPI.Services.Contracts;

namespace TariffCalcAPI.TariffCalcAPI.Services
{
    public class ProductService : Service<Product>, IProductService
    {
        private readonly IProductRepository Repository;

        public ProductService(IProductRepository repository) : base(repository)
        {
            Repository = repository;
        }

        public ICollection<ProductPricingDto> CalculatePricing(ProductCalculatePricingDto productCalculatePricingDto)
        {
            return Repository.CalculatePricing(productCalculatePricingDto);
        }
    }
}
