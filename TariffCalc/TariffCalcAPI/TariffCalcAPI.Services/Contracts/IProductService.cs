﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TariffCalcAPI.Dto;
using TariffCalcAPI.TariffCalcAPI.Models;

namespace TariffCalcAPI.TariffCalcAPI.Services.Contracts
{
    public interface IProductService : IService<Product>
    {
        //Add addictional methods to CRUD here...
        ICollection<ProductPricingDto> CalculatePricing(ProductCalculatePricingDto productCalculatePricingDto);
    }
}
