﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TariffCalcAPI.TariffCalcAPI.Services.Contracts
{
    public interface IService<TEntityModel> where TEntityModel : class
    {
        TEntityModel Get(int id);
        IEnumerable<TEntityModel> GetAll();
        bool Add(TEntityModel entity);
        void Update(TEntityModel entity);
        bool Delete(int id);
    }
}
