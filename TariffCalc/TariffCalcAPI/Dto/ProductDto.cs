﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TariffCalcAPI.TariffCalcAPI.Models.Enums;

namespace TariffCalcAPI.Dto
{
    public class ProductDto
    {
    }

    public class ProductCalculatePricingDto : ProductDto
    {
        public ProductCategory ProductCategory { get; set; }
        public double QuantityConsumption { get; set; }
        public int Months { get; set; }
    }

    public class ProductPricingDto : ProductDto
    {
        public string ProductName { get; set; }
        public double AnnualCost { get; set; }
    }
}
