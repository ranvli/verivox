﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TariffCalcWeb.Models;
using TariffCalcWeb.Util;

namespace TariffCalcWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult CalculateTariff(string consumption)
        {
            ProductCalculatePricingDto productCalculatePricingDto = new ProductCalculatePricingDto();
            productCalculatePricingDto.Months = 12; //i will hardcode this because of the PDF instructions but can be customziable
            productCalculatePricingDto.ProductCategory = 0; //0=electricity
            productCalculatePricingDto.QuantityConsumption = Convert.ToDouble(consumption);

            var response = ApiCall.Instance.ProductPriceCalculatorWithHttpMessagesAsync(productCalculatePricingDto);
            var prices = response.Result.Body.Data;
            prices = prices.OrderBy(p => p.AnnualCost).ToList();
            return PartialView("_PriceList", prices);
        }

    }
}