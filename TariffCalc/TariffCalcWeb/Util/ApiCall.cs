﻿using Microsoft.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace TariffCalcWeb.Util
{
    internal class ApiCall
    {
        internal static TariffCalcWebClient Instance
        {
            get
            {
                var s = new TariffCalcWebClient(new Uri(Constants.BaseURI_API), new TokenCredentials("Anonymous", "Anonymous"));
                return s;
            }
        }
    }
}